# t7s_parody_rhythm_game

## 概要
  この作品は、[tokyo 7th sisters](http://t7s.jp/)のスマートフォンアプリ内に登場するミニゲームを、可能な限り再現する目的で実装しました<br>
  元となるゲームは[こちら](https://www.youtube.com/watch?v=XdMxRfzH8EE&feature=youtu.be&t=48)をご参照ください<br>


### 特徴
- コーディングではオブジェクト指向を活用
- グラデーション部分は数値計算により実現
- 随時作成されるオブジェクトは木構造により保持
- 背景の空間は[Autodesk Fusion 360](https://www.autodesk.co.jp/products/fusion-360/overview)によりモデリング


# 開発/動作環境
- OS: Ubuntu 18.04 LTS
- Processing: processing-3.5.4-linux64
- Modeling: Autodesk Fusion 360


# 本リポジトリの取得
  ```
  cd hoge #任意のディレクトリに移動
  git clone https://gitlab.com/T.backup/t7s_parody_rhythm_game.git
  cd t7s_parody_rhythm_game
  RHYTHM=$PWD
  ```


# Processing本体のインストール
  ```
  cd $RHYTHM
  sudo apt update
  sudo apt upgrade
  wget https://download.processing.org/processing-3.5.4-linux64.tgz
  sudo tar -xvzof processing-3.5.4-linux64.tgz
  cd $RHYTHM/processing-3.5.4
  sudo ./install.sh
  # 以下のコマンドを実行し正常に動作すれば成功
  #./processing
  ```


# サンプルプログラムの実行
  ```
  cd $RHYTHM/processing-3.5.4
  ./processing $RHYTHM/codes/sample/sample.pde
  ```


# 必要なデータの取得

  ***画像の個人利用やダウンロードに関する法律は日々変化しています***<br>
  ***以下の処理は利用者自身の責任において実行して下さい***<br>
  ```
  cd $RHYTHM/datas
  wget https://pics.prcm.jp/903ed469ead55/58337365/png/58337365.png -O charaR.png
  convert -background 'rgba(0,0,0,0)' -rotate 5 charaR.png charaL.png
  convert charaL.png -crop 522x782+0+0 charaL.png
  ```


# ゲーム開始
  ```
  cd $RHYTHM/processing-3.5.4
  ./processing ../codes/rhythm_game/rhythm_game.pde
  ```


## ゲームの遊び方
  **ゲーム概要**<br>
  ボールを操作して、右から来る壁にうまく当てましょう<br>
  左上にどれだけ当たったかの記録が出ます<br>
  **操作方法**<br>
  Spaceキーをクリックする事でボールが上方向に移動します<br>
  押下中は上昇し、放すと下降します<br>


# 今後の改善点
  - ボールの進行後に出る残像を音符の画像で出力
  - FINISHの出力後にEXCELENT等の特典に対する結果を出力
  - 球が壁に当たらなかった際にキャラの出力を変える


# 参考文献

* インストール関係<br>
[Processing のビルドとインストール（ソースコードを使用）（Ubuntu 上）](https://www.kkaneko.jp/tools/ubuntu/processing.html)<br>
[UbuntuでProcessingを使う](https://hombre-nuevo.com/animation/anime0002/)<br>

* コーディング関係<br>
[Processingでグラデーションを描画する](http://kyle-in-jp.blogspot.com/2009/11/processing_18.html)<br>
[Processingでオブジェクト指向プログラミング (1)](https://yoppa.org/proga10/1190.html)<br>
[第7回 Processing実習 3 : Processingで画像データを扱う – 画像の分析・再合成](https://yoppa.org/geidai_media1_17/8243.html)<br>
[Processingの基礎 図形や楕円などを表示させてみよう](https://blanche-toile.com/web/processing-cg-design-basic)<br>
[Processingをはじめよう (Make: PROJECTS)](http://arms22.blog91.fc2.com/?tag=Processing)<br>
[Processing基礎最速入門](https://www.catch.jp/wiki/index.php?Processing%B4%F0%C1%C3%BA%C7%C2%AE%C6%FE%CC%E7)<br>

* データ関係<br>
[ナナシス 透過](https://prcm.jp/list/%E3%83%8A%E3%83%8A%E3%82%B7%E3%82%B9%20%E9%80%8F%E9%81%8E?page=4)<br>
