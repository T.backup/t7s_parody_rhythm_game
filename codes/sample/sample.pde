void setup() {
  size(200, 200);
  background(255);
}
  
void draw() {
  fill(0, 0, 0);
  ellipse(100, 100, 30, 30);  
  ellipse(150, 50, 30, 30);
  ellipse(150, 150, 30, 30);
  ellipse(50, 50, 30, 30);
  ellipse(50, 150, 30, 30);
  noFill();
  rect(10, 10, 180, 180);
}
