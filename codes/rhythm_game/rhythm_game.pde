PImage imgBG;
PImage imgR;
PImage imgL;

int state = 0;
int count_RL = 0;
int ing_BGx = 300;
int ing_BGy = 100;
int ballMoveLineX = 370;
int ballDiameter = 20;
int wallWidth = 20;
int wallHeight = 50;
int wall_count = 0;
int wallObject_count = 0;
int wallendWidth = 10;
int scorePosition = 30;
int scoreWidth = 600;
int scoreHeight = 20;
int scoreBlock = 20;
int hitCount = 0;
int human = 1;
int humanCheck = 1;

Ball ball = new Ball();
Wall head = new Wall((int)random(350)+100);;
WallEnd wallend = new WallEnd();

Wall r;
Wall s;

void setup(){
  imgBG = loadImage("../../datas/background.png");
  imgR = loadImage("../../datas/charaR.png");
  imgL = loadImage("../../datas/charaL.png");

  frameRate(120);
  size(900, 500);

  //rayer1 背景(写真を使用)
  image(imgBG, 0, 0, width, height);

  //reyer2 ingの背景
  ingBackground();

  //rayer4.0 得点表示 外枠
  fill(255, 255, 255, 150);
  stroke(255, 255, 255);
  strokeWeight(3);
  rect(scorePosition, scorePosition, scoreWidth, scoreHeight);
}

void draw(){

  if(state == 0){
    writeText("start");
    state = 1;
  }

  else if(state == 1){
    delay(3000);
    state = 2;
  }

  else if(state == 2){
    //ing内での球や壁の残像の初期化
    //描画の順番の都合上最初に記述する
    ingBackground();

    //rayer2.3 球
    ball_position();

    //rayer2.4 壁
    wall_position(ball.ball_y);

    //rayer3 球を追う音符

    //当たり判定
    r = head;
    s = r.next;

    hitCheck(ball, r, head, "head");
    while(true){
      if(s == null){
        break;
      }
      hitCheck(ball, s, r, "");
      r = r.next;
      s = s.next;
    }

    //rayer4.1 現在の得点
    int nowScore = 0;
    int scoreColor = 0;
    noStroke();

    while(nowScore < hitCount){
      fill(255 - scoreColor, 255, 255 - scoreColor, 255);
      rect(scorePosition + nowScore * scoreBlock, scorePosition,
           scoreBlock, scoreBlock);
      scoreColor = scoreColor + 255 / 30;
      nowScore++;
    }

    //rayer5 人物(透過イラストを使用)
    if(humanCheck == 1){
      character();
    }
  }

  else if(state == 3){
    delay(1500);
    state = 4;
  }

  else if(state == 4){
    writeText("finish");
    state = 5;
  }

  else if(state == 5){
    while(true){
      // ゲーム終了
    }
  }
}


void character(){
  if(human != 0){
    if(human == 1){
      charaReset();
      image(imgR, 100, 150, 200, 250);
      humanCheck = 0;
    }
    else{
      charaReset();
      image(imgL, 100, 150, 200, 250);
      humanCheck = 0;
    }
  }
  else{
    charaReset();
    image(imgR, 100, 150, 200, 250);
    humanCheck = 0;
  }
}


void charaReset(){
  //rayer1 背景(写真を使用)
  image(imgBG, 0, 0, width, height);
  //reyer2 ingの背景
  ingBackground();
  //rayer4.0 得点表示 外枠
  fill(255, 255, 255, 150);
  stroke(255, 255, 255);
  strokeWeight(3);
  rect(scorePosition, scorePosition, scoreWidth, scoreHeight);
}

void ingBackground(){
  //reyer2.0 ingの背景
  strokeWeight(6);
  stroke(0, 255, 255, 255);
  rect(300, 100, 500, 350);
  background_color();

  //rayer2.1 横線
  strokeWeight(1);
  stroke(255, 255, 255, 255);
  for(int i=150 ;i <= 400; i = i+50){
    line(300, i, 800, i);
  }

  //rayer2.2 通過後部分のクロスハッチ
  stroke(255, 255, 255, 255);
  for(int i=100; i+70<=450; i=i+10){
    line(300, i, ballMoveLineX, i+70);
  }
  for(int i=100; i+70<=450; i=i+10){
    line(ballMoveLineX, i, 300, i+70);
  }
  strokeWeight(4);
  line(ballMoveLineX, 100, ballMoveLineX, 450);
}

void background_color(){
  //グラデーションでのingのバックグラウンド
  color c1 = color(#4788db);
  color c2 = color(#9fff10);
  //  color c2 = color(#FFFFFF); これは白
  //  color c2 = color(#000000); これは黒

  int sx = ing_BGx + 500;
  int sy = ing_BGy + 350;

  float dr = (red(c2)   - red(c1))   / sy;
  float dg = (green(c2) - green(c1)) / sy;
  float db = (blue(c2)  - blue(c1))  / sy;

  for(int ly=ing_BGy; ly<sy; ly++){
    color pc = color((red(c1)+ly*dr),(green(c1)+ly*dg), (blue(c1)+ly*db));
    for(int lx=ing_BGx; lx<sx; lx++){
      set(lx,ly,pc);
    }
  }
}

void ball_position(){
  //スペースキーによるボールの位置変化
  if(keyPressed){
    if(100 < ball.ball_y - ballDiameter / 2 && key==' '){
      ball.ball_y = ball.ball_y - 2;
    }
  }
  else if(ball.ball_y + ballDiameter / 2 < 450){
    ball.ball_y = ball.ball_y + 2;
  }

  //ボールの描画の呼び出し
  ball.print_ball();
}

class Ball{
  //ボールの初期位置を範囲の平均=中間に指定
  int ball_x = ballMoveLineX;
  int ball_y = (100+450)/2;

 //ボールの描画
  void print_ball(){
    fill(255, 0, 255, 255);
    stroke(255, 255, 255);
    strokeWeight(3);
    smooth();
    ellipse(ball_x, ball_y, ballDiameter, ballDiameter);
  }
}

void wall_position(int wall_y){

  //ゲーム終了
  if(!(wallObject_count < 30)){
    //白線終了
    if(wallend.wallend_x< 300){
      //ingBackground();
      //stateを次のfinish画面へ
      state = 3;
    }
    else{
      wallend.print_wallend();
      wallend.wallend_x--;
    }
  }
  //100～500の乱数 < wall_count で、カウントが増える毎に出現率が上がる
  else if((int)random(100, 500) < wall_count){
    wall_count = 0;
    Wall p = head;
    while(p.next != null){
      p = p.next;
    }
    p.next = new Wall(wall_y);
    wallObject_count++;
  }

  //左端に到達したらオブジェクトを消す
  //ついでにキャラの体勢の変更
  if(head.wall_x < 300){
    head = head.next;
    human = 0;
    humanCheck = 1;
  }

  Wall q = head;
  while(q.next != null){
    q.print_wall();
    q.wall_x--;
    q = q.next;
  }
  wall_count++;
}

class Wall{

  //xの座標は右端で固定
  int wall_x = 800 - wallWidth;
  int wall_y;
  //ツリーの構造にする
  Wall next;
  //wallの色を決めておく
  int wallColor;

  Wall(int wall_y){
    //変数randomを用意し 0～150の乱数を入れる
    int random = (int)random(150);
    int adjust = (int)random(100);

    //ついでに色も此処で決める
    wallColor = random;

    //if文を使って random%2 の結果により正か負を決定する
    if(random%2 == 1){
      random = -random;
    }

    //this.wall_y に wall_y+random の値を代入する
    this.wall_y = wall_y + random;

    //この時条件分岐で画面外に出ないようにする
    if(450 < this.wall_y + wallHeight){
      this.wall_y = 450 - wallHeight - adjust;
    }
    else if(this.wall_y < 100){
      this.wall_y = 100 + adjust;
    }
  }

 //迫り来る壁の描画
  void print_wall(){
    //ballに触れずに線を超えたら薄く
    if(ballMoveLineX < this.wall_x + wallWidth){
      fill(255, 0, wallColor, 255);
    }
    else{
      fill(255, 0, wallColor, 170);
    }
    stroke(255, 255, 255);
    strokeWeight(3);
    strokeJoin(BEVEL);
    rect(this.wall_x, this.wall_y, wallWidth, wallHeight);
  }
}

class WallEnd{

  int wallend_x = 800 - wallendWidth;

  void print_wallend(){
    fill(255, 255, 255, 255);
    stroke(255, 255, 255);
    strokeWeight(3);
    strokeJoin(BEVEL);
    rect(wallend_x, 100, wallendWidth, 350);
  }
}

//当たり判定
void hitCheck(Ball ballHit, Wall wallHit, Wall wallHitBefore, String str){

  if(( wallHit.wall_x < ballHit.ball_x + ballDiameter / 2
     &&ballHit.ball_x - ballDiameter / 2 < wallHit.wall_x + wallWidth
     &&wallHit.wall_y < ballHit.ball_y
     &&ballHit.ball_y < wallHit.wall_y + wallHeight)
     ||
      (wallHit.wall_x < ballHit.ball_x
     &&ballHit.ball_x - ballDiameter / 2 < wallHit.wall_x + wallWidth
     &&wallHit.wall_y < ballHit.ball_y + ballDiameter / 2
     &&ballHit.ball_y < wallHit.wall_y)
     ||
      (wallHit.wall_x < ball.ball_x
     &&ballHit.ball_x - ballDiameter / 2 < wallHit.wall_x + wallWidth
     &&ballHit.ball_y - ballDiameter / 2 < wallHit.wall_y + wallHeight
     &&wallHit.wall_y + wallHeight < ballHit.ball_y)){

       //当たり判定による壁の消滅
       //headの時
       if(str == "head"){
         //wallHitBefore = wallHit.next;
         head = head.next;
       }
       //それ以外の二つめ以降の時
       else{
         wallHitBefore.next = wallHit.next;
       }

       //得点
       hitCount++;

       //キャラの体勢の変更
       if(human == 0){
         human = 1;
       }
       else{
         human = -human;
       }
       //変化を伝える
       humanCheck = 1;
  }
}

void writeText(String txt){
  //フォントを決める
  PFont font = createFont("Velvenda Cooler", 500);
  textFont(font);

  fill(0, 0, 255);
  textSize(180);
  textAlign(CENTER);
  //縁取り
  text(txt, width/2+5, height/2+50+5);
  text(txt, width/2-5, height/2+50+5);
  text(txt, width/2+5, height/2+50-5);
  text(txt, width/2-5, height/2+50-5);
  //金色
  fill(230, 180, 34);
  textSize(180);
  textAlign(CENTER);
  text(txt, width/2, height/2+50);
}
